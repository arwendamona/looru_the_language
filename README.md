# Lo'oru

This is the language spoken by Lo'orish People.
The Lo'orish are part of the larger Eluria World.

For an introduction to the grammar of Lo'orish, have a look at [the reference grammar](./description/looru_an_introduction.pdf).

If you want to have a look at the created vocabulary, you can check [the lexicon](./lexicon/lexicon.pdf) out.

## Configuration

Virtual environment name: `conlang`
Vocabulary should be defined into `../vocabulary/`.

## Package Management

**Make sure you have activated the virtual environment before updating the dependencies!**

```Bash
pip3 freeze --local -r requirements.txt > requirements.txt
```

## AVailable Commands

### Check Valid Vocabulary

```Bash
python3 vocabulary_checker.py
```

### Generate Lexicon

```Bash
python3 generate_lexicon.py
```

### Generate Possible Lo'oru Words

Will only generate words that are not already present in the vocabulary.

```Bash
python3 generate_words.py
```
