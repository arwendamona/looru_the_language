## The following requirements were added by pip freeze:
attrs==18.2.0
cycler==0.10.0
gnuplotlib==0.28
jsonschema==3.0.0
kiwisolver==1.0.1
numpy==1.16.2
numpysane==0.17
pyparsing==2.3.1
pyrsistent==0.14.11
python-dateutil==2.8.0
six==1.12.0
