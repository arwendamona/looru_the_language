import copy
import random

import alphabet
import vocabulary_checker
from probabilities import probabilities


def create_word_from_lexical_category(values, throw_errors=True):
    """
    Returns a word of the correct type based (inferred from the value of the 'lexical_category' attribute)
    containing the information passed in values
    Assume that values represent a valid word.

    Throws error if the lexical_attribute found in values is unknown.
    Setting throw_errors to False will make this function return a Word object instead.
    """

    if values['lexical_category'] == 'noun':
        return Noun(values)
    elif values['lexical_category'] == 'verb':
        return Verb(values)
    elif values['lexical_category'] == 'adjective':
        return Adjective(values)
    elif values['lexical_category'] == 'pronoun':
        return Pronoun(values)
    elif values['lexical_category'] == 'relative pronoun':
        return RelativePronoun(values)
    elif values['lexical_category'] == 'article':
        return Article(values)
    elif values['lexical_category'] == 'adverb':
        return Adverb(values)
    elif values['lexical_category'] == 'preposition':
        return Preposition(values)
    elif values['lexical_category'] == 'conjunction':
        return Conjunction(values)
    elif values['lexical_category'] == 'interjection':
        return Interjection(values)
    else:
        if throw_errors:
            raise Exception('Unknown lexical_class: ' + str(values))
        else:
            return Word(values)


class Word:
    """
    The base class for all words.
    """

    def __init__(self, values):
        """
        Creates a word from the given values
        Assumes that values represents a correct word.
        """

        self.english = set(values['english'])
        self.looru = values["looru"]
        self.syllables = values['syllables']
        self.lexical_category = values['lexical_category']

        self.grammatical_usage = values.get('grammatical_usage', '')
        self.meaning = values.get('meaning', '')
    
    def __repr__(self):
        out = '<' + self.full_lexical_category().capitalize() + ': ' + str(self.english) + ' -> "' + self.looru + '"'
        if self.meaning != '':
            out += ', ' + self.meaning
        out += '>'
        return out

    def __getitem__(self, item):
        return self.__dict__[item]

    def flatten(self) -> list:
        """
        Returns a list of Word containing this word flattened on the english attribute.
        """
        flattened_word = []
        for english_translation in self.english:
            # Using shallow copy will save a lot of time and memory and is sufficient if values are not modified
            shallow_copy = copy.copy(self)

            shallow_copy.main_english_meaning = english_translation

            shallow_copy_translations = copy.copy(shallow_copy.english)
            shallow_copy_translations.remove(english_translation)
            shallow_copy.other_english_meanings = shallow_copy_translations

            flattened_word.append(shallow_copy)
        return flattened_word

    def full_lexical_category(self) -> str:
        return self.lexical_category


class Noun(Word):
    """
    The base class for all nouns.
    """
    def __init__(self, values):
        super().__init__(values)
        self.noun_class = values['noun']['noun_class']
        self.countability = values['noun']['countability']

    def full_lexical_category(self):
        return 'noun ' + self.noun_class + ' ' + self.countability


class Verb(Word):
    """
    The base class for all verbs.
    """
    def __init__(self, values):
        super().__init__(values)
        self.type = values['verb']['type']

    def full_lexical_category(self):
        out = 'verb '
        if self.type == 'intransitive':
            out += 'intransitive'
        if self.type == 'transitive':
            out += 'transitive'
        return out


class Adjective(Word):
    """
    The base class for all adjective.
    """
    def __init__(self, values):
        super().__init__(values)


class Article(Word):
    """
    The base class for all articles.
    """
    def __init__(self, values):
        super().__init__(values)
        self.definiteness = values['article']['definiteness']
        self.animacy = values['article']['animacy']

    def full_lexical_category(self):
        return 'article ' + self.definiteness + ' ' + self.animacy


class Pronoun(Word):
    """
    The base class for all pronoun.
    """
    def __init__(self, values):
        super().__init__(values)
        self.person = values['pronoun']['person']
        self.plurality = values['pronoun']['plurality']
        self.animacy = values['pronoun']['animacy']
        self.case = values['pronoun']['case']

    def full_lexical_category(self):
        return 'pronoun ' + self.person + ' ' + self.plurality + ' ' + self.animacy + ' ' + self.case


class RelativePronoun(Word):
    """
    The base class for all relative pronoun.
    """
    def __init__(self, values):
        super().__init__(values)
        self.case = values['relative pronoun']['case']

    def full_lexical_category(self):
        return 'relative ' + self.case + ' pronoun '


class Adverb(Word):
    """
    The base class for all adverbs.
    """
    def __init__(self, values):
        super().__init__(values)

    def __repr__(self):
        return '<Adverb: "' + str(self.english) + '" / "' + self.looru + '">'


class Preposition(Word):
    """
    The base class for all prepositions.
    """
    def __init__(self, values):
        super().__init__(values)
        self.type = values['preposition']['type']

    def full_lexical_category(self):
        return 'preposition ' + self.type


class Conjunction(Word):
    """
    The base class for all conjunction.
    """
    def __init__(self, values):
        super().__init__(values)
        self.type = values['conjunction']['type']

    def full_lexical_category(self):
        return 'conjunction ' + self.type


class Interjection(Word):
    """
    The base class for all interjections.
    """
    def __init__(self, values):
        super().__init__(values)


class Syllable:
    """
    In Lo'oru, syllables are of the form (((C) C) C) V (V) (V).
    Each of those possible letters has associated probability for each category of consonants respectively vowel.
    Valid syllables cannot have two identical consonants in them.

    This object encapsulate this information.
    """
    
    # Consonants distributions are ordered from the consonant closest to the nucleus to the further
    consonant_distributions = [
        # Consonant closest to the nucleus
        probabilities.DiscreteProbabilityDistribution([
            {
                'value': alphabet.FRICATIVES,
                'probability': 1,
            },
            {
                'value': alphabet.NASALS,
                'probability': 2,
            },
            {
                'value': alphabet.APPROXIMANTS,
                'probability': 7,
            },
        ]),
        # Middle consonant
        probabilities.DiscreteProbabilityDistribution([
            {
                'value': alphabet.FRICATIVES,
                'probability': 5,
            },
            {
                'value': alphabet.NASALS,
                'probability': 3,
            },
            {
                'value': alphabet.APPROXIMANTS,
                'probability': 2,
            },
        ]),
        # Consonant further from the nucleus
        probabilities.DiscreteProbabilityDistribution([
            {
                'value': alphabet.AFFRICATES,
                'probability': 9,
            },
            {
                'value': alphabet.FRICATIVES,
                'probability': 1,
            },
        ]),
    ]

    consonant_number_distr = probabilities.DiscreteProbabilityDistribution([
        {
            'value': 1,
            'probability': 5,
        },
        {
            'value': 2,
            'probability': 4,
        },
        {
            'value': 3,
            'probability': 1,
        },
    ])

    vowel_number_distr = probabilities.DiscreteProbabilityDistribution([
        {
            'value': 1,
            'probability': 4,
        },
        {
            'value': 2,
            'probability': 4,
        },
        {
            'value': 3,
            'probability': 2,
        },

    ])

    @staticmethod
    def create_new_syllable():
        consonant_number = Syllable.consonant_number_distr.sample()
        vowel_number = Syllable.vowel_number_distr.sample()

        syllable = ''

        # Add consonant (in reverse order)
        used_consonant = []
        for i in range(consonant_number):
            # Sample a consonant
            consonant_type = Syllable.consonant_distributions[i].sample()
            new_consonant = random.choice(consonant_type).looru_symbol

            # Make sure it wasn't already used in the syllable
            while new_consonant in used_consonant:
                consonant_type = Syllable.consonant_distributions[i].sample()
                new_consonant = random.choice(consonant_type).looru_symbol

            used_consonant.append(new_consonant)
            syllable = new_consonant + syllable

        # TODO: Have more complicated proba distribution for vowels than uniform?
        # Add vowels
        for i in range(vowel_number):
            syllable += random.choice(alphabet.VOWELS).looru_symbol

        return syllable, {'consonants': consonant_number, 'vowels': vowel_number}
    
    def __init__(self, syllable: str):
        self.syllable = syllable


class WordGenerator:

    ending_consonants_number_distr = probabilities.DiscreteProbabilityDistribution([{
        'value': 0,
        'probability': 10
    }, {
        'value': 1,
        'probability': 2
    }, {
        'value': 2,
        'probability': 1
    }])

    ending_consonants_distribution = probabilities.DiscreteProbabilityDistribution([{
        'value': alphabet.FRICATIVES,
        'probability': 1,
    },
    {
        'value': alphabet.NASALS,
        'probability': 2,
    },
    {
        'value': alphabet.APPROXIMANTS,
        'probability': 2,
    }])

    syllable_number_distr = probabilities.DiscreteProbabilityDistribution([
        {
            'value': 1,
            'probability': 4,
        },
        {
            'value': 2,
            'probability': 3,
        },
        {
            'value': 3,
            'probability': 2,
        },
        {
            'value': 4,
            'probability': 1,
        },
    ])

    @staticmethod
    def create_new_looru_word(word, vocabulary: list):
        word, stats = WordGenerator._create_new_looru_word(word)
        print(word)
        while not vocabulary_checker.VocabularyChecker.check_new_looru_word(word, vocabulary):
            word, stat = WordGenerator._create_new_looru_word(word)
        return word, stats

    @staticmethod
    def _create_new_looru_word(word: dict):
        syllable_number = WordGenerator.syllable_number_distr.sample()

        stats = {
            'consonants': 0,
            'vowels': 0
        }

        word['looru'] = ''
        word['syllables'] = ''

        # Syllables
        i = 0
        while True:
            syllable, syllable_stats = Syllable.create_new_syllable()
            word["looru"] += syllable
            if word["syllables"] != '':
                word["syllables"] += '.'
            word["syllables"] += alphabet.from_looru_to_ipa(syllable)
            for key, value in syllable_stats.items():
                stats[key] += value
            i += 1
            if i >= syllable_number:
                break

        # Ending consonants
        ending_consonants_number = WordGenerator.ending_consonants_number_distr.sample()
        if ending_consonants_number > 0:
            word['syllables'] += '.'
        for i in range(ending_consonants_number):
            consonant_type = WordGenerator.ending_consonants_distribution.sample()
            new_consonant = random.choice(consonant_type).looru_symbol
            word['looru'] += new_consonant
            word['syllables'] += alphabet.from_looru_to_ipa(new_consonant)
            stats['consonants'] += 1

        stats['syllables'] = syllable_number

        return word, stats
