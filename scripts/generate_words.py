
import time
import json

import alphabet
import vocabulary
import vocabulary_parser
import sonorant_graph
from pretty_output import ConsoleColor, clear_screen


def get_english_translations():
    print('\n\nEnter comma separated list of english translations for the word to create (ex: big, tall, giant)')
    return [translation.strip().lower() for translation in input().split(',')]


if __name__ == "__main__":

    voc = vocabulary_parser.VocabularyParser(validate=True)

    stats = []

    with open("./vocabulary/format.json") as file:
        format = json.load(file)
    what_lexical_category_string = 'What kind of lexical category do you want to define? '
    lexical_category_shortcuts = '['
    shortcut_to_lexical_category_mapping = {}

    for entry in format['word_categories']:
        shortcut = entry['shortcut']
        category = entry['name']
        shortcut_to_lexical_category_mapping[shortcut] = category
        what_lexical_category_string += category + '/'
        lexical_category_shortcuts += shortcut + '/'
    lexical_category_shortcuts += '] '
    what_lexical_category_string += ' ' + lexical_category_shortcuts

    user_input = ''
    while user_input != 'e':
        new_word = {}

        clear_screen()
        lexical_category_shortcut = ''
        while True:
            lexical_category_shortcut = input(what_lexical_category_string)
            if lexical_category_shortcut in shortcut_to_lexical_category_mapping.keys():
                lexical_category = shortcut_to_lexical_category_mapping[lexical_category_shortcut]
                print('Creating a new ' + lexical_category)
                break;
            else:
                print(ConsoleColor.RED + 'Invalid value!' + ConsoleColor.END)

        new_word['lexical_category'] = lexical_category
        new_word[lexical_category] = {}

        english_traductions = get_english_translations()
        new_word['english'] = english_traductions

        prompt = 'Generating loorish ' + lexical_category + ' for '
        for traduction in english_traductions:
            prompt += traduction + ', '

        # Generate Looru word
        is_word_accepted = ''
        while is_word_accepted not in ['y', 'Y', 's', 'S']:

            # User requested to type the word himself/herself
            if is_word_accepted in ['t', 'T']:
                user_typed_word = input(
                    ConsoleColor.BOLD
                    + "Lo'oru word (syllables must be separated using dots or apostrophe): "
                    + ConsoleColor.END
                ).lower()
                new_word['looru'] = ''.join([char for char in user_typed_word if char != '.'])
                new_word['syllables'] = alphabet.from_looru_to_ipa(user_typed_word)
                stat = {}
            else:
                new_word, stat = vocabulary.WordGenerator.create_new_looru_word(new_word, voc.vocabulary)

            clear_screen()
            print(ConsoleColor.GREEN + ConsoleColor.BOLD + prompt + ConsoleColor.END)
            print('\nProposition:\t' + ConsoleColor.BOLD + new_word["looru"] + ' (' + new_word['syllables'] + ')'
                  + ConsoleColor.END + '\n\nStats:')
            for key, value in stat.items():
                print('\t- ' + str(key) + ': ' + str(value))
            print('\n')
            sonorant_graph.SonorantGraph.print_ascii_sonorant_graph(new_word['looru'])
            time.sleep(0.01)
            print('\n')
            is_word_accepted = input('Accept the proposition? yes/no/stop/type text [y/N/s/t] ')

        if is_word_accepted in ['s', 'S']:
            break

        print('\n\n' + ConsoleColor.GREEN + ConsoleColor.BOLD + 'Accepted' + ConsoleColor.END)

        # Add missing mandatory properties
        print(ConsoleColor.BOLD + '\n\nYou must now complete some mandatory properties:' + ConsoleColor.END)
        mandatory_properties = format['properties_to_define']['mandatory'][lexical_category]
        for entry in mandatory_properties:
            prompt = "\n\nEnter value for mandatory property '" + entry["name"] + "' "
            if entry.get('possible_values', None) is not None:
                for possible_value in entry['possible_values']:
                    prompt += possible_value + '/'
                prompt += ' '

                property_value = ''
                while True:
                    print(prompt)
                    print(ConsoleColor.YELLOW + entry['helper'] + ConsoleColor.END)
                    property_value = input()
                    if property_value not in entry['possible_values']:
                        print(ConsoleColor.RED + 'Invalid value! Please enter valid value.' + ConsoleColor.END)
                    else:
                        break
            else:
                print(prompt)
                print(ConsoleColor.YELLOW + entry['helper'] + ConsoleColor.END)
                property_value = input()
            new_word[lexical_category][entry['name']] = property_value

        # Add optional properties
        print(ConsoleColor.BOLD + '\n\nYou can now complete some optional properties:' + ConsoleColor.END)
        optional_properties = format['properties_to_define']['optional']['all']
        for entry in optional_properties:
            prompt = "\n\nEnter value for optional property '" + entry["name"]\
                     + "' (leave blank if you don't want to fill the property) "

            if entry.get('possible_values', None) is not None:
                possible_values = []
                for possible_value in entry['possible_values']:
                    prompt += possible_value + '/'
                    possible_values.append(possible_value)

                property_value = ''
                while True:
                    print(prompt)
                    print(ConsoleColor.YELLOW + entry['helper'] + ConsoleColor.END)
                    property_value = input()
                    if property_value not in possible_values and property_value != '':
                        print(ConsoleColor.RED + 'Invalid value! Please enter valid value.' + ConsoleColor.END)
                    else:
                        break
            else:
                print(prompt)
                print(ConsoleColor.YELLOW + entry['helper'] + ConsoleColor.END)
                property_value = input()

            if property_value != '':
                new_word[entry['name']] = property_value

        optional_properties = format['properties_to_define']['optional'][lexical_category]
        for entry in optional_properties:
            prompt = "\n\nEnter value for optional property '" + entry["name"]\
                     + "' (leave blank if you don't want to fill the property) "

            if entry.get('possible_values', None) is not None:
                possible_values = []
                for possible_value in entry['possible_values']:
                    prompt += possible_value + '/'
                    possible_values.append(possible_value)

                property_value = ''
                while True:
                    print(prompt)
                    print(ConsoleColor.YELLOW + entry['helper'] + ConsoleColor.END)
                    property_value = input()
                    if property_value not in possible_values and property_value != '':
                        print(ConsoleColor.RED + 'Invalid value! Please enter valid value.' + ConsoleColor.END)
                    else:
                        break
            else:
                print(prompt)
                print(ConsoleColor.YELLOW + entry['helper'] + ConsoleColor.END)
                property_value = input()

            if property_value != '':
                new_word[lexical_category][entry['name']] = property_value

        vocabulary_file_path = './vocabulary/words/words.json'
        with open(vocabulary_file_path) as output_vocabulary:
            v = json.load(output_vocabulary)
            v.append(new_word)

        with open(vocabulary_file_path, 'w') as output_vocabulary:
            output_vocabulary.write(json.dumps(v, sort_keys=True, indent=4))

        print('\nYour new word was saved into the vocabuary file ' + vocabulary_file_path)
        user_input = input("\n\nWhat should I do now? exit/define new word [e/D]: ")

    # TODO: Print useful stats on generated numbers
