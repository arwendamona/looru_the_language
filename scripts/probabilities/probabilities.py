
import random

class DiscreteProbabilityDistribution():
    """
    Represents a discrete probability distribution.

    Constructor should be provided with an object of the form [{"value": <value>, "probability": <probability>}, ...]
    Where value can be anything and <probability> is a number.

    The probability that a specific value is obtained when sampling the distribution is <probability> over the sum of all <probability>
    """

    def __init__(self, distr: list):
        self.distr = []

        bound = 0
        for entry in distr:
            self.distr.append({
                'lower_bound': bound,
                'value': entry['value']
            })
            bound += entry['probability']
        self.max = bound

    def sample(self):
        r = random.uniform(0, self.max)
        previous_element = self.distr[0]
        for entry in self.distr[1:]:
            if r < entry['lower_bound']:
                return previous_element['value']
            previous_element = entry
        return previous_element['value']