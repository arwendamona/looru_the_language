import os
import json
import jsonschema

import alphabet

SCHEMA_PATH = os.path.dirname(__file__) + '/../vocabulary/schema.json'


class VocabularyChecker:

    def __init__(self):
        """
        Check schema file exists
        """
        print('Creating vocabulary checker object.')
        if not os.path.exists(SCHEMA_PATH):
            raise Exception('Schema file for vocabulary not found! The following file is missing: ' + str(SCHEMA_PATH))
        print('Found schema specification for json vocabulary (' + SCHEMA_PATH + ')')

        with open(SCHEMA_PATH) as schema_file:
            self.schema = json.load(schema_file)
            jsonschema.Draft4Validator.check_schema(self.schema)
        print('Schema is valid.')

    def check_vocabulary(self, voc):
        """
        Check that passed vocabulary is valid with
        respect to the schema of the vocabulary.
        Throws exceptions if this is not the case
        """
        print('Checking vocabulary is valid (i.e. matching the json schema).')
        jsonschema.validate(voc, self.schema)
        print('Input vocabulary is valid. Continuing checks...')

        print("Checking that the Lo'oru words defined are all valid.")
        self._check_words_are_valid_in_looru(voc)
        print("All Lo'oru words are unique. Continuing checks...")

        print("Checking that the Lo'oru words defined are all unique.")
        self._check_words_are_unique_in_looru(voc)
        print("All Lo'oru words are unique. Continuing checks...")

        print('Checking that the English words defined are all unique.')
        self._check_words_are_unique_in_english(voc)
        print('All English words are unique.')

        print('All checks passed! Your vocabulary is valid.')

    def _check_words_are_valid_in_looru(self, voc: list) -> None:
        for word in voc:
            if not self._first_looru_syllable_is_valid(word):
                raise Exception("Invalid first Lo'oru syllable for word " + str(word))
            if not self._last_looru_syllable_is_valid(word):
                raise Exception("Invalid last Lo'oru syllable for word " + str(word))

    def _check_words_are_unique_in_looru(self, voc: list) -> None:
        i = 0
        exempted_lexical_categories = ['noun', 'verb']
        while i < len(voc) - 1:
            j = i + 1
            while j < len(voc):
                if voc[i]["looru"] == voc[j]["looru"] and\
                    not (voc[i]["lexical_category"] in exempted_lexical_categories and voc[j]['lexical_category'] in exempted_lexical_categories):
                    raise Exception(
                        "Following word was defined twice in Lo'oru: " +
                        voc[i]['looru'] + '\n[1]: ' +
                        str(voc[i]) + '\n[2]: ' + str(voc[j])
                    )
                j += 1
            i += 1

    @staticmethod
    def _first_looru_syllable_is_valid(word):
        invalid_beginnings = [
            # transformation
            "tsu'a",
            'na.ra',
            'na.ru'
            # negation
            'rié',
            'ri.é'
        ]
        looru_syllables = alphabet.from_ipa_to_looru(word['syllables'])
        for invalid_beginning in invalid_beginnings:
            if looru_syllables.startswith(invalid_beginning):
                return False

        case_beginnings = [
            'wé',
            'wé.a',
            'lua'
        ]
        if word['lexical_category'] != 'pronoun':
            for invalid_beginning in case_beginnings:
                if looru_syllables.startswith(invalid_beginning):
                    return False
        
        invalid_noun_beginnings = [
            'tsu.a'
        ]
        if word['lexical_category'] == 'noun':
            for invalid_beginning in invalid_noun_beginnings:
                if looru_syllables.startswith(invalid_beginning):
                    return False

        return True

    @staticmethod
    def _last_looru_syllable_is_valid(word):
        looru_syllables = alphabet.from_ipa_to_looru(word['syllables'])
        if word['lexical_category'] != 'pronoun':
            plural_endings = ['yé', 'yo', 'yi', 'yu']
            for invalid_ending in plural_endings:
                if looru_syllables.endswith(invalid_ending):
                    return False

        invalid_verb_endings = ['la', 'vé', 'vé.a']
        if word['lexical_category'] == 'verb':
            for invalid_verb_ending in invalid_verb_endings:
                if looru_syllables.endswith(invalid_verb_ending):
                    return False

        invalid_adjective_endings = ['nlo', 'nli', 'nlu', 'flo', 'fli', 'flu']
        if word['lexical_category'] == 'adjective':
            for invalid_adjective_ending in invalid_adjective_endings:
                if looru_syllables.endswith(invalid_adjective_ending):
                    return False

        return True

    @staticmethod
    def check_new_looru_word(new_word, voc: list) -> bool:

        if not VocabularyChecker._first_looru_syllable_is_valid(new_word):
            return False

        if not VocabularyChecker._last_looru_syllable_is_valid(new_word):
            return False

        # Check word isn't already defined in Lo'oru
        for word in voc:
            if new_word['looru'] == word['looru']:
                return False

        return True
            
    def _check_words_are_unique_in_english(self, voc: list) -> None:
        i = 0
        while i < len(voc) - 1:
            # Skip words that have no direct translations in English
            if len(set(voc[i]["english"])) is 0 or set(voc[i]["english"]) == set(['']):
                i += 1
                continue
            # Skip pronouns as many of them have the exact same translation (intentionnaly)
            if voc[i]['lexical_category'] == 'pronoun' or voc[i]['lexical_category'] == 'article':
                i += 1
                continue
            j = i + 1
            while j < len(voc):
                if set(voc[i]["english"]) == set(voc[j]["english"]) and voc[i]["lexical_category"] == voc[j]["lexical_category"]:
                    raise Exception(
                        "Following word was defined multiple times in English: " +
                        str(voc[i]['english']) + '\n[1]: ' +
                        str(voc[i]) + '\n[2]: ' + str(voc[j])
                    )
                j += 1
            i += 1


if __name__ == "__main__":
    import vocabulary_parser
    vocabulary_parser.VocabularyParser(validate=True)
    print('Vocabulary checked successfully')
