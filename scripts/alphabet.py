import enum


class LetterType(enum.Enum):
    VOWEL = 'vowel',
    APPROXIMANT = 'approximant',
    NASAL = 'nasal',
    FRICATIVE = 'fricative',
    AFFRICATE = 'affricate',
    PUNCTUATION = 'punctuation'


class Letter:
    def __init__(self, looru_symbol: str, ipa_symbol: str, latex_symbol: str, letter_type: LetterType, sonority: float):
        self.looru_symbol = looru_symbol
        self.ipa_symbol = ipa_symbol
        self.latex_symbol = latex_symbol
        self.sonority = sonority
        self.type = letter_type

        LETTERS.append(self)
        LOORU_TO_LETTER[looru_symbol] = self
        IPA_TO_LETTER[ipa_symbol] = self

    def __repr__(self):
        return '<' + str(self.type) + ' letter: ' + self.looru_symbol + ' / ' + self.ipa_symbol + '>'


LOORU_TO_LETTER = {}
IPA_TO_LETTER = {}
LETTERS = []

VOWELS = [
    Letter('a', 'a', 'a', LetterType.VOWEL, 20),
    Letter('é', 'e', 'e', LetterType.VOWEL, 19),
    Letter('è', 'ɛ', '\\textipa{E}', LetterType.VOWEL, 19),
    Letter('o', 'o', 'o', LetterType.VOWEL, 19),
    Letter('i', 'i', 'i', LetterType.VOWEL, 18),
    Letter('u', 'u', 'u', LetterType.VOWEL, 18),
    Letter('ü', 'y', 'y', LetterType.VOWEL, 18),
]

APPROXIMANTS = [
    Letter('y', 'j', 'j', LetterType.APPROXIMANT, 15),
    Letter('w', 'w', 'w', LetterType.APPROXIMANT, 15),
    Letter('l', 'l', 'l', LetterType.APPROXIMANT, 15),
    # TODO: Decide how 'r' will be used
    Letter('r', 'x', 'x', LetterType.APPROXIMANT, 15),
    Letter('v', 'β', '\\textipa{B}', LetterType.APPROXIMANT, 15),
]

NASALS = [
    Letter('m', 'm', 'm', LetterType.NASAL, 10),
    Letter('n', 'n', 'n', LetterType.NASAL, 10),
]

FRICATIVES = [
    Letter('f', 'ɸ', '\\textipa{F}', LetterType.FRICATIVE, 5),
    Letter('þ', 'θ', '\\textipa{T}', LetterType.FRICATIVE, 5),
    Letter('s', 's', 's', LetterType.FRICATIVE, 5),
    Letter('c', 'ʂ', '\\textipa{\:s}', LetterType.FRICATIVE, 5),
    Letter('h', 'h', 'h', LetterType.FRICATIVE, 5),
]

AFFRICATES = [
    Letter('ts', 'ʈs', '\\textipa{\:t}s', LetterType.AFFRICATE, 0),
    Letter('tc', 'ʈʂ', '\\textipa{\:t}\\textipa{\:s}', LetterType.AFFRICATE, 0),
]

PUNCTUATION = [
    Letter("'", '.', '.', LetterType.PUNCTUATION, -1),
    Letter('.', '.', '.', LetterType.PUNCTUATION, -1),
]


def from_looru_to_ipa(word: str) -> str:
    output = ''
    for letter in LetterParser.parse_looru_letter(word):
        letter_object = LOORU_TO_LETTER.get(letter, None)
        if letter_object is None:
            raise Exception("Following letter is not part of the Lo'oru alphabet: " + str(letter))
        output += letter_object.ipa_symbol
    return output


def from_ipa_to_looru(word: str) -> str:
    output = ''
    for letter in LetterParser.parse_ipa_letter(word):
        letter_object = IPA_TO_LETTER.get(letter, None)
        if letter_object is None:
            raise Exception("Following letter is not part of the IPA alphabet: " + str(letter) + ' in ' + word)
        output += letter_object.looru_symbol
    return output


def from_ipa_to_latex(word: str) -> str:
    output = ''
    for letter in LetterParser.parse_ipa_letter(word):
        letter_object = IPA_TO_LETTER.get(letter, None)
        if letter_object is None:
            raise Exception("Following letter is not part of the Lo'oru alphabet: " + str(letter))
        output += letter_object.latex_symbol
    return output


class LetterParser:

    @staticmethod
    def parse_looru_letter(word: str) -> list:
        letters = []
        prefix = ''
        for letter in word:
            if letter == 't':
                prefix = 't'
                continue
            letters.append(prefix + letter)
            prefix = ''

        return letters

    @staticmethod
    def parse_ipa_letter(word: str) -> list:
        letters = []
        prefix = ''
        for letter in word:
            if letter == 'ʈ':
                prefix = 'ʈ'
                continue
            letters.append(prefix + letter)
            prefix = ''

        return letters
