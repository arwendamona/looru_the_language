
import random
import re
import vocabulary_parser
from pretty_output import ConsoleColor, clear_screen


if __name__ == "__main__":

    voc = vocabulary_parser.VocabularyParser(validate=True).vocabulary

    user_input = ''
    while user_input != 'e':

        clear_screen()

        user_input = input(
            ConsoleColor.BOLD
            + 'What do you want to do? search english/search looru/search lexical category/reload vocabulary/random/quit [E/l/c/r/a/q] '
            + ConsoleColor.END
        )

        if user_input in ['e', 'E', '']:
            pattern = input('\n' + ConsoleColor.BOLD + 'Enter pattern to match: ' + ConsoleColor.END)
            print()

            found_items = 0
            prog = re.compile(pattern)
            for word in voc:
                for translation in word.english:
                    if re.fullmatch(prog, translation) is not None:
                        print()
                        print(word)
                        found_items += 1
            print()
            print('Found ' + str(found_items) + ' items.\n\n')

        elif user_input in ['l', 'L']:
            pattern = input('\n' + ConsoleColor.BOLD + 'Enter pattern to match: ' + ConsoleColor.END)
            print()

            found_items = 0
            prog = re.compile(pattern)
            for word in voc:
                if re.fullmatch(prog, word.looru) is not None:
                    print()
                    print(word)
                    found_items += 1
            print()
            print('Found ' + str(found_items) + ' items.\n\n')

        elif user_input in ['c', 'C']:
            lexical_category = input('\n' + ConsoleColor.BOLD + 'Enter lexical category to match: ' + ConsoleColor.END)
            print()

            found_items = 0
            for word in voc:
                if word.lexical_category == lexical_category:
                    print()
                    print(word)
                    found_items += 1
            print()
            print('Found ' + str(found_items) + ' items.\n\n')

        elif user_input in ['r', 'R']:
            voc = vocabulary_parser.VocabularyParser(validate=True).vocabulary
            print('\nVocabulary reloaded!')

        elif user_input in ['a', 'A']:
            lexical_category = input('\n' + ConsoleColor.BOLD + 'Enter a lexical category: ' + ConsoleColor.END)
            print()

            already_printed = []
            for i in range(5):
                word = random.choice([word for word in voc if word['lexical_category'] == lexical_category and word not in already_printed])
                print(str(word))
                already_printed.append(word)
            print()

        elif user_input in ['q', 'Q']:
            break

        else:
            print(ConsoleColor.RED + 'Unrecognized option.' + ConsoleColor.END)

        # Wait before clearing the screen that user types something
        input(ConsoleColor.BOLD + 'Continue? ' + ConsoleColor.END)

