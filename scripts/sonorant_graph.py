
import subprocess
import numpy as np

import alphabet

class SonorantGraph():

    @staticmethod
    def print_ascii_sonorant_graph(word: str):
        x_values = range(len(word))
        x_tics = '(' + ','.join(['"' + letter + '" ' + str(num) for letter, num in zip(word, range(len(word)))]) + ')\n'
        xtics_string = 'set xtics ' + x_tics

        y_values = []

        for letter in alphabet.LetterParser.parse_looru_letter(word):
            y_values.append(alphabet.LOORU_TO_LETTER[letter].sonority)
        
        gnuplot = subprocess.Popen(["/usr/bin/gnuplot"], 
                                stdin=subprocess.PIPE)
        gnuplot.stdin.write(b"set term dumb 80 25\n")
        gnuplot.stdin.write(b"set key off\n")
        gnuplot.stdin.write(b"set xrange [-1:" + bytes(str(len(word)), 'utf-8') + b"]\n")
        gnuplot.stdin.write(bytes(xtics_string, 'utf-8'))
        gnuplot.stdin.write(b"set yrange [-1:21]\n")
        gnuplot.stdin.write(b"plot '-' with linespoints\n")
        for i,j in zip(x_values,y_values):
            gnuplot.stdin.write(bytes(str(i) + ' ' + str(j) + '\n', 'utf-8'))
        gnuplot.stdin.write(b"e\n")
        gnuplot.stdin.write(b"quit\n")
        gnuplot.stdin.flush()
        gnuplot.wait()
