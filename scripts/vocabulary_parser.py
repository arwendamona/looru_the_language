
import os
import glob
import json

import vocabulary_checker
import vocabulary

VOCABULARY_FOLDER = os.path.dirname(__file__) + '/../vocabulary/'


class VocabularyParser:

    def __init__(self, validate=True):
        """
        Finds all vocabulary file (all files ending in .json) in the
        vocabulary folder, parses them, check they are valid
        (unless specified otherwise) and store them.

        Valid vocabularies must define words that are not already existing in looru
        """

        self.vocabulary = []

        # Contains the flatten list of all words found in all vocabularies
        aggregated_vocabulary = []
        for file_path in glob.glob(VOCABULARY_FOLDER + '**/*.json'):
            print('Found vocabulary file: ' + file_path)

            with open(file_path) as file:
                new_vocabulary = json.load(file)
            print('Vocabulary file loaded.')
            aggregated_vocabulary.extend(new_vocabulary)

        # Check that the vocabulary is valid
        if validate:
            voc_checker = vocabulary_checker.VocabularyChecker()
            voc_checker.check_vocabulary(aggregated_vocabulary)

        # Create Word objects and save vocabulary
        for new_word in aggregated_vocabulary:
            self.vocabulary.append(
                vocabulary.create_word_from_lexical_category(new_word))
        
        print('Loaded ' + str(len(self.vocabulary)) + ' words from the vocabulary folder.')
