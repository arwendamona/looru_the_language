
import os
from contextlib import contextmanager

import alphabet
import vocabulary
import vocabulary_parser

LEXICON_FOLDER = os.path.dirname(__file__) + '/../lexicon/'
LEXICON_FILE_NAME = 'lexicon.tex'

LATEX_PREAMBULE_FILE = LEXICON_FOLDER + 'lexicon_preambule.tex'
LATEX_ABBREVIATIONS_LIST = LEXICON_FOLDER + 'abbreviation_list.tex'


@contextmanager
def cd(new_directory):
    previous_directory = os.getcwd()
    os.chdir(os.path.expanduser(new_directory))
    try:
        yield
    finally:
        os.chdir(previous_directory)


class LexiconGenerator:

    LEXICAL_CATEGORY_TO_ABBR = {
        'adjective': 'adj.',
        'adverb': 'adv.',
        'article': 'art.',
        'definite': 'def.',
        'indefinite': 'indf.',
        'aetherical': '\\ae th.',
        'animated': 'ani.',
        'inanimated': 'ina.',
        'noun': 'noun.',
        'nominative': 'nom.',
        'accusative': 'acc.',
        'dative': 'dat.',
        'genitive': 'gen.',
        'unspecified': '',
        'pronoun': 'pro.',
        'non-1st': 'n1',
        '1st': '1',
        'singular': 'sg.',
        'trial': 'tri.',
        'paucal': 'pau.',
        'greater_plural': 'gpl.',
        'collective': 'col.',
        'verb': '',
        'transitive': 'vt.',
        'intransitive': 'vi.',
        'preposition': 'prep.',
        'conjunction': 'cnj.',
        'countable': 'cnt.',
        'uncountable': 'uncnt.',
        'inanimate': 'ina.',
        'animate': 'ani.',
        'aetherical': '\\ae th.',
        'interjection': 'intrjctn',
        'relative pronoun': 'rel'
    }

    @staticmethod
    def get_abbr_from_word(word: vocabulary.Word):
        abbreviations = LexiconGenerator.get_abbr_or_error(word.lexical_category)

        if word.lexical_category == 'verb':
            abbreviations += ' ' + LexiconGenerator.get_abbr_or_error(word.type)

        elif word.lexical_category == 'article':
            abbreviations += LexiconGenerator.get_abbr_or_error(word.definiteness)
            abbreviations += LexiconGenerator.get_abbr_or_error(word.animacy)

        elif word.lexical_category == 'pronoun':
            abbreviations += LexiconGenerator.get_abbr_or_error(word.person)
            abbreviations += LexiconGenerator.get_abbr_or_error(word.plurality)
            abbreviations += LexiconGenerator.get_abbr_or_error(word.case)
            abbreviations += LexiconGenerator.get_abbr_or_error(word.animacy)

        elif word.lexical_category == 'noun':
            abbreviations += LexiconGenerator.get_abbr_or_error(word.noun_class)
            abbreviations += LexiconGenerator.get_abbr_or_error(word.countability)

        return abbreviations[:-1]

    @staticmethod
    def get_abbr_or_error(key: str) -> str:
        abbr = LexiconGenerator.LEXICAL_CATEGORY_TO_ABBR.get(key, None)
        if abbr is None:
            raise Exception("The given lexical category does not exist in the abbreviation table: " + key)
        return abbr

    def __init__(self, v: vocabulary_parser.VocabularyParser):
        # Flatten vocabulary on english translations
        self.vocabulary = v.vocabulary
        self.flattened_vocabulary = [flattened_word for word in v.vocabulary for flattened_word in word.flatten()]

    @staticmethod
    def _generate_looru_lexicon_entry(word: vocabulary.Word) -> str:
        output_string = '\\dictentry{\\textbf{' + word.looru + '}\\ [' + alphabet.from_ipa_to_latex(word.syllables)\
                        + ']}[\\textbf{' + word.looru + '}]{' + ', '.join(sorted(word.english)) + '}{' + LexiconGenerator.get_abbr_from_word(word) + '}'
        if word.grammatical_usage != '':
            output_string += '[' + word.grammatical_usage + ']'
        if word.meaning != '':
            if word.grammatical_usage != '':
                output_string += '[' + word.meaning + ']'
            else:
                output_string += '[][' + word.meaning + ']'
        output_string += '\n'
        return output_string

    @staticmethod
    def _generate_english_lexicon_entry(word: vocabulary.Word) -> str:
        output_string = '\\dictentry{\\textbf{' + word.main_english_meaning + '}'
        if len(word.other_english_meanings) is not 0:
            output_string += ', ' + ', '.join(sorted(word.other_english_meanings))
        output_string += '}'
        if len(word.other_english_meanings) is not 0:
            output_string += '[\\textbf{' + word.main_english_meaning + '}]'
        output_string += '{' + word.looru + '\\ [' + alphabet.from_ipa_to_latex(word.syllables) + ']}'
        output_string += '{' + LexiconGenerator.get_abbr_from_word(word) + '}'
        if word.grammatical_usage != '':
            output_string += '[' + word.grammatical_usage + ']'
        if word.meaning != '':
            if word.grammatical_usage != '':
                output_string += '[' + word.meaning + ']'
            else:
                output_string += '[][' + word.meaning + ']'
        output_string += '\n'
        return output_string

    @staticmethod
    def _generate_lexicon_entry_for(language: str, word: vocabulary.Word):
        if language == 'looru':
            return LexiconGenerator._generate_looru_lexicon_entry(word)
        elif language == 'english':
            return LexiconGenerator._generate_english_lexicon_entry(word)
        else:
            raise Exception('Unrecognized language: ' + language)

    def _generate_language_lexicon_chapter(self, from_language_name: str, to_language_name: str) -> str:
        return "\\chapter*{" + from_language_name + ' $\\rightarrow$ ' + to_language_name + "}\n"\
                     + "\\addcontentsline{toc}{chapter}{" + from_language_name + ' $\\rightarrow$ ' + to_language_name\
                     + "}\n\n"

    def _generate_looru_lexicon_chapter_content(self):
        returned_string = ''
        previous_first_letter = -1
        for word in sorted(self.vocabulary, key=lambda w: w.looru):
            if str.lower(word.looru[:1]) != previous_first_letter:
                if previous_first_letter != -1:
                    returned_string += '\\end{multicols}\n\n'
                returned_string += '\\section{' + str.upper(word.looru[:1]) + '}\n\n\\begin{multicols}{2}\n'
                previous_first_letter = str.lower(word.looru[:1])

            returned_string += LexiconGenerator._generate_looru_lexicon_entry(word)

        returned_string += "\n\n\\end{multicols}"

        return returned_string

    def _generate_english_lexicon_chapter_content(self):
        returned_string = ''
        previous_first_letter = -1
        for word in sorted(self.flattened_vocabulary, key=lambda w: w.main_english_meaning):
            if str.lower(word.main_english_meaning[:1]) != previous_first_letter:
                if previous_first_letter != -1:
                    returned_string += '\\end{multicols}\n\n'
                returned_string += '\\section{' + str.upper(word.main_english_meaning[:1])\
                                   + '}\n\n\\begin{multicols}{2}\n'
                previous_first_letter = str.lower(word.main_english_meaning[:1])

            returned_string += LexiconGenerator._generate_english_lexicon_entry(word)

        returned_string += "\n\n\\end{multicols}"

        return returned_string

    def generate(self):
        """
        Take a list of vocabulary.Word as input and create a lexicon using latex
        """

        with open(LATEX_PREAMBULE_FILE) as f:
            latex_code = f.read()

        with open(LATEX_ABBREVIATIONS_LIST) as f:
            latex_code += f.read()

        latex_code += self._generate_language_lexicon_chapter("Lo'orü", 'English')
        latex_code += self._generate_looru_lexicon_chapter_content()
        latex_code += self._generate_language_lexicon_chapter("English", "Lo'orü")
        latex_code += self._generate_english_lexicon_chapter_content()

        latex_code += "\n\n\\end{document}\n"

        if not os.path.exists(LEXICON_FOLDER):
            os.mkdir(LEXICON_FOLDER)
        
        with open(LEXICON_FOLDER + LEXICON_FILE_NAME, 'w') as f:
            f.write(latex_code)
        print('LaTeX file for lexicon was generated in ' + LEXICON_FOLDER + LEXICON_FILE_NAME)
        
        print('Attempting to compile tex file to pdf now...')
        with cd(LEXICON_FOLDER):
            # we are in ~/Library
            os.system('lualatex ' + LEXICON_FILE_NAME)


if __name__ == "__main__":
    voc = vocabulary_parser.VocabularyParser(validate=True)
    LexiconGenerator(voc).generate()
    os.system('xdg-open ' + LEXICON_FOLDER + LEXICON_FILE_NAME[:-3] + 'pdf')
